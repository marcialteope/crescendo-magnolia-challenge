package com.crescendocollective.recipes;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class RecipeRestClientTest {
	
	RecipeRestClient recipeCmsClient;

	@Before
	public void setUp() throws Exception {
		recipeCmsClient = new RecipeRestClient();
	}

	@Test
	public void testGetRecipes() {
		List<Recipe> recipies = recipeCmsClient.getRecipes();
		System.out.println(recipies);
	}
}
