package com.crescendocollective.emailform;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmailFormController {
		
	@RequestMapping(method = RequestMethod.POST, value = "/joinList")
	public String joinMailingList(@ModelAttribute EmailForm form){
		return form.getFirstName();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/hello")
	public String hello(){
		return "hello magnolia world";
	}

}
