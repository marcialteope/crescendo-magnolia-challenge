package com.crescendocollective.emailform;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailService {

	public static void sendEmail(String toRecepient, String fromSender, String messagebody) {
		
		final String HOST = "localhost";
		final String PROTOCOL = "mail.smtp.host";
		Properties properties = System.getProperties();
		properties.setProperty(PROTOCOL, HOST);
		Session session = Session.getDefaultInstance(properties);

		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromSender));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toRecepient));
			message.setSubject("Join mailing list");
			message.setText(messagebody);
			Transport.send(message);
			System.out.println("Message sent successfully.");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

}