package com.crescendocollective.recipes;

import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import info.magnolia.context.MgnlContext;
import info.magnolia.repository.RepositoryConstants;

public class RecipePagesBuilder {
	
	public void buildPage(){
		
		Session session = null;
		try {
			session = MgnlContext.getJCRSession(RepositoryConstants.WEBSITE);
			Node foundNode = session.getNode("/recipes");
			System.out.println(foundNode.getName());
			foundNode.addNode("recipe");
			System.out.println(getRecipeData().size());
			session.save();
		} catch (RepositoryException e) {
			session.logout();
			e.printStackTrace();
		}finally{
			session.logout();
		}
	}
	
	private List<Recipe> getRecipeData(){
		RecipeRestClient client = new RecipeRestClient();
		List<Recipe> recipeList = client.getRecipes();
		return recipeList;
	}
}
